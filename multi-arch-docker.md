# How to use an Ubuntu 22.04 machine do multi-arch container builds?

## Intro
This tutorial will guide you in creating a container builder machine that is able to do multiarch builds for Linux.
We will use a simple Ubuntu 22.04 Server on a VPS as base, install Docker (as it is more popular — change it to Podman if you prefer) with `buildx` and QEMU for foreign platform emulation.

Creation of Ubuntu VPS, installing the Ubuntu Operating System and having `root` access will **NOT** be covered in this tutorial.

As this guide uses a Ubuntu 22.04 Server installed from scratch, the steps here can be replicated to your own home or work desktop Ubuntu 22.04 computer. Other machines with different linux distributions can use these steps here and adapt for replication in their different distributions.

## Summary
-   [System used in this tutorial](#system-used-in-this-tutorial)
-   [Creating the builder](#creating-the-multiarch-builder)
    -   [Before we start](#before-we-start)
    -   [Preparing the system](#preparing-the-system)
    -   [Installing Docker and Buildx](#installing-docker-and-buildx)
    -   [Installing QEMU](#installing-qemu-for-cross-platform-emulation)
    -   [Linking new architectures to Docker and Buildx/BuildKit](#linking-new-architectures-to-docker-and-buildxbuildkit)
    -   [Running foreign architectures](#running-foreign-architectures)
        -   [Docker Hello-World](#docker-hello-world)
        -   [Alpine Linux](#alpine-linux)
    -   [Building foreign architectures](#building-foreign-architectures)
-   [Final words](#final-words)


## System used in this tutorial
-   Ubuntu Server 22.04 VPS
    -   amd64 (aka x86-64) architecture
    -   4 vCPUs and 8 GB of RAM (essential for building and emulating other architectures)
    -   50GB NVMe SSD storage (faster storage results in faster builds)
-   Root access to Ubuntu Server

For this to work, the Docker minimum requirement includes Linux Kernel >= 4.8 and binfmt-support >= 2.1.7. Both, kernel and binfmt-support, are also available in Ubuntu 18.04 LTS and 20.04 LTS, so this means that this tutorial should work on Ubuntu 18.04 and 20.04, but this tutorial will only cover Ubuntu 22.04.

## Creating the multiarch builder

### Before we start
In this section, we will update the current system and then install `Docker`, `Docker Buildx` and `Qemu` packages.

-   _All scripts are executed as the `root` user_
-   _`apt <command> --yes` option is not used so in `apt` commands for interactiveness, you can optionally add them to remove prompts_
-   _`apt install --no-install-recommends` is widely used so only what is strictly required is installed. This avoids un-needed packages from being installed and headaches with package conflicts_


### Preparing the system
To start the things, we will update the system
```console
[root]# apt update
[root]# apt upgrade
```

After updating, we will install `ca-certificates`, `curl`, `gnupg` and `lsb-release` as they will be needed later when installing docker.
```console
[root]# apt install --no-install-recommends ca-certificats curl gnupg lsb-release
```

### Installing Docker and Buildx
[Docker has an installation tutorial](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository). We will use that as base. For more information, read the own Docker installation guide

First, we will add the Docker APT Repository
```console
[root]# mkdir --parents /etc/apt/keyrings
[root]# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
[root]# echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Second, we will install Docker. We will also install the `buildx` plugin as later it will be helpful.
Docker Compose plugin is optional, but we will install it because it is recommended by the Docker's own tutorial
```console
[root]# apt update
[root]# apt install --no-install-recommends docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

### Installing QEMU for cross-platform emulation
There are 2 options for installing QEMU
1.  [System emulation](https://www.qemu.org/docs/master/system/index.html) - In this mode, QEMU emulates a full system, including peripherals. It is more accurate but slower. It can be accelerated by hypervisors like KVM or Xen
2.  [Usermode emulation](https://www.qemu.org/docs/master/user/main.html) - In this mode, it is able to invoke a Linux compiled for a different architecture. There may be some incompatibility issues like some features not implemented and only Linux can be emulated

We will use Usermode emulation as we plan only to do multiarch builds for Linux distributions and full system emulation is not required.
```console
[root]# apt install --no-install-recommends qemu-user-static
```

After that, we need to configure the new additional binary formats for executables. For this, `binfmt-support` will be installed for this configuration and will also add a new `systemd` unit that will configure them on boot.
```console
[root]# apt install --no-install-recommends binfmt-support
```

From this point, the Ubuntu is able to emulate other architectures.
To display the architectures that can be emulated by QEMU:
```console
[root]# update-binfmts --display | grep ^qemu-
```
It should display something like this:
```text
qemu-aarch64:
qemu-alpha:
qemu-arm:
< ... trimmed for brevity ... >
qemu-xtensaeb:
```

### Linking new architectures to Docker and Buildx/BuildKit
Now that the OS is able to emulate other architectures, we need to let Docker and its builder know that they are available.
Building for foreign platforms is not supported by the legacy builder, so we will have to use the newer BuildKit builder that is available via Docker Buildx. Remember that we also installed the `buildx` Docker plugin when [we installed Docker](#installing-docker)? Here it is.

To setup the plugin, we need to
```console
[root]# docker buildx install
```

Now, with everything setup, to ensure that everything is correct, list the `buildx` builders available and their supported build architectures
```console
[root]# docker buildx ls
```
It should display someting like this:
```text
NAME/NODE DRIVER/ENDPOINT STATUS  BUILDKIT PLATFORMS
default * docker
  default default         running 23.0.4   linux/amd64, linux/amd64/v2, linux/amd64/v3, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/mips64le, linux/mips64, linux/arm/v7, linux/arm/v6
```
_The platforms listed in `PLATFORMS` may differ from depending on the host machine hardware, kernel and BIOS configuration._

### Running foreign architectures
Now that Docker and QEMU are installed and fully setup, why not trying to run the `Hello World` container from a different architecture? Let's try it:

#### Docker Hello-World
```console
[root]# docker run --rm --platform linux/arm/v7 hello-world:latest
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (arm32v7)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
...
```

#### Alpine Linux
```console
[root]# docker run --rm alpine:3.17 apk --print-arch
x86_64
[root]# docker run --rm --platform linux/arm64/v8 alpine:3.17 apk --print-arch
aarch64
[root]# docker run --rm --platform linux/arm/v7 alpine:3.17 apk --print-arch
armv7
```

### Building foreign architectures
Now that you are running foreign architectures, let's **build** foreign architectures

Let's use the following Dockerfile
```dockerfile
## File is placed in project/Dockerfile
FROM alpine:latest

RUN echo "My arch is: $(apk --print-arch)"
```

Time to build it for ARMv7. During the build, you should see it printing the `My arch is: armv7`
```console
[root project]# docker buildx build --platform linux/arm/v7 --tag my-image:tag-armv7 .
```
It is possible to build multiple images by passing multiple platforms separated by commas. We will build for 4 different platforms and assign all built images to a single multi-platform list
```console
[root project]# docker buildx build --jobs 4 --platform linux/amd64,linux/arm64/v8,linux/arm/v7,linux/arm/v6 --manifest image:multiarch .
```

## Final words
Now that you are able to do multi-arch builds, maybe you should consider to do this in cloud machine and follow these steps to create a GitLab CI runner that is able to do all that on a Continuous Integration machine hosted on the cloud.

This project itself uses custom GitLab CI runners with QEMU to build all `rdnxk` and `redemonbr` OCI and Docker images.
And then, building and pushing images are so easily done that can be done in YAML file:
```yml
build:
  stage: build
  image: redemonbr/podman-steroids:latest
  variables:
    IMAGE_REPO: docker.io/namespace/my-image
    REGISTRY_USER: my-user
    REGISTRY_PASSWORD: mysekritp@$$ ## this should come from a safe secret manager
    MANIFEST_NAME: example
    PLATFORMS: linux/amd64,linux/i386,linux/arm64/v8,linux/arm/v7
  before_script:
    - podman login --username $REGISTRY_USER --password $REGISTRY_PASSWORD docker.io
  script:
    - podman build --squash --jobs $(nproc) --platform $PLATFORMS --manifest $IMAGE_REPO:$MANIFEST_NAME .
    - podman manifest push $IMAGE_REPO:$MANIFEST_NAME $IMAGE_REPO:$MANIFEST_NAME
```

And if you are wondering, Podman Steroids itself uses Podman Steroids for building and publishing the images. It even runs on a weekly schedule for updating all images

In the future, a new tutorial will show how to use this Ubuntu Server 22.04 VPS multiarch builder machine with a GitLab Runner to build multi-arch images with Podman or Docker.
