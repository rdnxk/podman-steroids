#!/usr/bin/env sh

## Inputs (optional), TARGETPLATFORM (ex: 'linux/amd64' | 'linux/arm64/v8' | 'linux/arm/v7' )
docker_arch="${1:-$TARGETPLATFORM}"
docker_arch="${docker_arch#*/}"

apk_arch="$(apk --print-arch)"

## convert apk arch to docker arch
case "$apk_arch" in
    aarch64)
        arch=arm64/v8 ;;
    x86_64)
        arch=amd64 ;;
    *)
        arch="$apk_arch" ;;
esac

[ -z "$arch" ] && echo "undetected or unsupported arch" && exit 1

if [ "$arch" != "$docker_arch" ]; then
    echo "Architectures do not match: ARCH=$apk_arch ; TARGET=$docker_arch"
    exit 1
else
    echo "Architectures match: ARCH=$apk_arch ; TARGET=$docker_arch"
fi
