#!/usr/bin/env sh

set -e

github_project="$1"

curl --fail --location --silent \
    -H "Accept: application/json" \
    "https://github.com/$github_project/releases/latest" \
    | jq -r '.tag_name'
